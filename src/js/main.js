import {Water} from "./water";
import {Seaweed} from "./seaweed";
import {Fish} from "./fish";
import {Predatory} from "./predatory";

const COUNT_AREA = 10, // размер матрицы m x n
    SEAWEED = 10, // кол-во водорослей
    HEARB_FISH = 7, // кол-во травоядных рыб
    PREDATORY_FISH = 5; // кол-во хищных рыб

class Main {

    constructor() {
        this.countArea = COUNT_AREA;
        this.seaweed = SEAWEED;
        this.herbFish = HEARB_FISH;
        this.predatoryFish = PREDATORY_FISH;
        this.virtualArea = []; // виртуальное поле моделирования
        this.fish = []; // массив классов травоядных рыб
        this.predatory = []; // массив классво хищных рыб
    }

    /**
     * Обределяем рандомное значение
     * @param min
     * @param max
     * @returns {number}
     */
    randomInteger(min, max) {
        let rand = min - 0.5 + Math.random() * (max - min + 1);
        rand = Math.round(rand);

        return rand;
    };

    /**
     * Изменяем класс в виртуальное поле моделирования
     * @param obj
     */
    changeVirtualArea(obj) {
        this.virtualArea[obj.x].splice(obj.y, 1, obj);
    };

    /**
     * Отрисовываем поле матрицы с объектами
     */
    render() {
        const area = document.querySelector('.app');

        while (area.firstChild) {
            area.removeChild(area.firstChild);
        }

        this.virtualArea.forEach((item) => {
            const row = document.createElement('div');

            row.className = 'row';
            item.forEach((obj) => {
                const cell = document.createElement('div');
                cell.className = 'cell';

                if (obj.hasOwnProperty('type')) {
                    cell.classList.add(obj.type)
                }

                row.appendChild(cell);
            });

            area.appendChild(row);
        });

    };

    /**
     * Создаем виртуальное поле матрицы
     */
    createdVirtualArea() {
        for (let i = 0; i < this.countArea; i++) {
            let virtualRow = [];
            for (let j = 0; j < this.countArea; j++) {
                virtualRow.push(new Water(i, j))
            }
            this.virtualArea.push(virtualRow)
        }
    };

    /**
     * Создаем и добавляем в виртуальную модель классы водорослей и рыб
     * @param count
     * @param type
     */
    createdObjects(count, type) {
        for (let i = 0; i < count; i++) {
            const x = this.randomInteger(0, this.countArea - 1),
                y = this.randomInteger(0, this.countArea - 1);

            if (!this.virtualArea[x][y].hasOwnProperty('type')) {
                if (type === 'fish') {
                    let fishObj = new Fish(x, y, type);
                    this.changeVirtualArea(fishObj);
                    this.fish.push(fishObj)
                } else if (type === 'predatory') {
                    let predatoryObj = new Predatory(x, y, type);
                    this.changeVirtualArea(predatoryObj);
                    this.predatory.push(predatoryObj)
                } else {
                    this.changeVirtualArea(new Seaweed(x, y, type));
                }
            } else {
                i--
            }
        }
    };

    /**
     * Проверяем наличие травоядных рыб в радиусе одного шага от хищьной рыбы
     * @param coordinates
     * @returns {*}
     */
    scan(coordinates) {
        for (let i = coordinates.left; i < this.virtualArea.length; i++) {
            if (i > coordinates.right) {
                break;
            }
            for (let j = coordinates.top; j < this.virtualArea[i].length; j++) {
                if (j > coordinates.down) {
                    break;
                }

                if (this.virtualArea[i][j].type === 'fish') {
                    return this.virtualArea[i][j];
                }
            }
        }
    };

    /**
     * Метод передвижения рыб
     * @param arr
     */
    stepFish(arr) {
        for (let i = 0; i < arr.length; i++) {
            let prevX = arr[i].x,
                prevY = arr[i].y,
                coordinates = arr[i].getCoordinates(this.countArea - 1),
                nextX = this.randomInteger(coordinates.left, coordinates.right),
                nextY = this.randomInteger(coordinates.top, coordinates.down),
                newFish = arr[i].changeNewFish();

            if (!arr[i].checkTimeLife()) {
                this.changeVirtualArea(new Water(prevX, prevY));
                arr.splice(i, 1);
                i--;

                continue;
            }

            if (arr[i].type === 'predatory') {
                let eats = this.scan(coordinates);
                if (eats) {
                    this.fish.forEach((item, index) => {
                        if (item === eats) {
                            this.fish.splice(index, 1);
                            Object.defineProperties(arr[i], {
                                'x': {value: eats.x},
                                'y': {value: eats.y}
                            });
                            arr[i].setNewStarvation();
                            this.changeVirtualArea(arr[i]);
                            this.changeVirtualArea(new Water(prevX, prevY));

                        }
                    });

                    continue;
                } else {
                    if (!arr[i].checkStarvation()) {
                        this.changeVirtualArea(new Water(prevX, prevY));
                        arr.splice(i, 1);
                        i--;

                        continue;
                    }
                }
            }

            if (!this.virtualArea[nextX][nextY].hasOwnProperty('type')) {
                let newObj = Object.assign(arr[i], this.virtualArea[nextX][nextY]);

                if (newFish <= 0) {
                    let newFish = arr[i].type === 'fish' ? new Fish(prevX, prevY, 'fish') : new Predatory(prevX, prevY, 'predatory');

                    this.changeVirtualArea(newFish);
                    arr.push(newFish);
                    arr[i].setNewFish();
                } else {
                    this.changeVirtualArea(new Water(prevX, prevY));
                }
                this.changeVirtualArea(newObj);
            }
        }
    };

    timeNextStep() {
        this.interval = setInterval(() =>{
            this.nextStep();
        },5000)
    }

    clearTimeNextStep() {
        clearInterval(this.interval)
    }

    nextStep() {
        this.stepFish(this.predatory);
        this.stepFish(this.fish);
        this.render();
    };

    start() {
        document.querySelector('.step').addEventListener('click', () => {
            this.clearTimeNextStep();
            this.nextStep();
            this.timeNextStep();
        });

        this.createdVirtualArea();
        this.createdObjects(this.seaweed, 'seaweed');
        this.createdObjects(this.herbFish, 'fish');
        this.createdObjects(this.predatoryFish, 'predatory');
        this.render();
        this.timeNextStep();
    }
}

const game = new Main();
game.start();