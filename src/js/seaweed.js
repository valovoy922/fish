import {Water} from "./water";

export class Seaweed extends Water{
    constructor(x, y, type) {
        super(x, y);
        this.type = type;
    }
}