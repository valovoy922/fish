import {Fish} from "./fish";

const STARVATION = 4; // период голодной смерти

export class Predatory extends Fish {

    constructor(x, y, type) {
        super(x, y, type);
        this.starvation = STARVATION;
    };

    checkStarvation() {
        return --this.starvation;
    };

    setNewStarvation() {
        this.starvation = STARVATION;
    }
}