import {Seaweed} from "./seaweed";

const TIME_LIFE = 10; // период жизни
const NEW_FISH = 5; // период размножения

export class Fish extends Seaweed {
    constructor(x, y, type) {
        super(x, y, type);
        this.timeLife = TIME_LIFE;
        this.newFish = NEW_FISH;
    };

    getCoordinates(countArea) {
        return {
            left: this.x === 0 ? this.x : this.x - 1,
            right: this.x === countArea ? this.x : this.x + 1,
            top: this.y === 0 ? this.y : this.y - 1,
            down: this.y === countArea ? this.y : this.y + 1,
        }
    };

    checkTimeLife() {
        return this.timeLife--;
    }

    setNewFish() {
        this.newFish = NEW_FISH;
    }

    changeNewFish() {
        return this.newFish--;
    }
}